﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TshirtOrderForm.aspx.cs" Inherits="Assignment1.TshirtOrderForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assignment 1</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ValidationSummary ID="formValidationSummary" runat="server" ForeColor="Red"/>
        <section>
            <header>
                <h1>T-Shirt Printing Service</h1>
            </header>
        </section>
        <section>
            <fieldset>
                <legend>Personal Information</legend>
                <div>
                    <asp:Label ID="fNameLbl" runat="server" AssociatedControlID="clientFirstName" Text="First Name:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientFirstName" placeholder="Enter your first name"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your first name" ControlToValidate="clientFirstName" ID="fnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                <div>
                </div>
                    <asp:Label ID="lNameLbl" runat="server" AssociatedControlID="clientLastName" Text="Last Name:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientLastName" placeholder="Enter your last name"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your last name" ControlToValidate="clientLastName" ID="lnValidate" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label ID="emailLbl" runat="server" AssociatedControlID="clientEmail" Text="Email Address:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientEmail" placeholder="e.g. yourname@mail.com"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your email address" ControlToValidate="clientEmail" ID="emailValidator1" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ValidationExpression="^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$" ErrorMessage="Email is not valid" ControlToValidate="clientEmail" ID="emailValidator2" ForeColor="Red"></asp:RegularExpressionValidator>
                    <!-- reg exp code taken from http://regexlib.com/Search.aspx?k=email&AspxAutoDetectCookieSupport=1 -->
                </div>
                <div>
                    <asp:Label ID="numLbl" runat="server" AssociatedControlID="clientPhoneNumber" Text="Phone Number:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientPhoneNumber" placeholder="e.g. 6471234567"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your phone number" ControlToValidate="clientPhoneNumber" ID="numberValidator1" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$" ErrorMessage="Phone number is not valid" ControlToValidate="clientPhoneNumber" ID="numberValidator2" ForeColor="Red"></asp:RegularExpressionValidator>
                    <!-- reg exp code taken from https://stackoverflow.com/questions/16699007/regular-expression-to-match-standard-10-digit-phone-number -->
                </div>
                <div>
                    <asp:Label ID="addressLbl" runat="server" AssociatedControlID="clientDeliveryAddress" Text="Address:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientDeliveryAddress" placeholder="e.g. 123 Haight St."></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your address" ControlToValidate="clientDeliveryAddress" ID="addressValidator" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label ID="cityLbl" runat="server" AssociatedControlID="clientCity" Text="City:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientCity" placeholder="e.g. Toronto"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your city" ControlToValidate="clientCity" ID="cityValidator" ForeColor="Red"></asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:Label ID="provinceLbl" runat="server" AssociatedControlID="clientProvince" Text="Province:"></asp:Label>
                    <asp:DropDownList runat="server" ID="clientProvince">
                        <asp:ListItem Value="AB" Text="Alberta"></asp:ListItem>
                        <asp:ListItem Value="BC" Text="British Columbia"></asp:ListItem>
                        <asp:ListItem Value="MB" Text="Manitoba"></asp:ListItem>
                        <asp:ListItem Value="NB" Text="New Brunswick"></asp:ListItem>
                        <asp:ListItem Value="NL" Text="Newfoundland and Labrador"></asp:ListItem>
                        <asp:ListItem Value="NS" Text="Nova Scotia"></asp:ListItem>
                        <asp:ListItem Value="ON" Text="Ontario"></asp:ListItem>
                        <asp:ListItem Value="PE" Text="Prince Edward Island"></asp:ListItem>
                        <asp:ListItem Value="QC" Text="Quebec"></asp:ListItem>
                        <asp:ListItem Value="SK" Text="Saskatchewan"></asp:ListItem>
                    </asp:DropDownList>
                    <br/>
                </div>
                <div>
                    <asp:Label ID="clientZipCodeLbl" runat="server" AssociatedControlID="clientZipCode" Text="Zip Code:"></asp:Label>
                    <asp:TextBox runat="server" ID="clientZipCode" placeholder="e.g. LK6 4W1"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter your zip code" ControlToValidate="clientZipCode" ID="zipCodeValidator" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ValidationExpression="[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ] ?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]" ErrorMessage="Zip code is not valid" ControlToValidate="clientZipCode" ID="zipCodeRegExValidator" ForeColor="Red"></asp:RegularExpressionValidator>
                    <!-- source for canadian post code regex https://stackoverflow.com/questions/1146202/canadian-postal-code-validation -->
                </div>
           </fieldset>
       </section>
       <section>
            <fieldset>
                <legend>Shirt Design</legend>
                 <div>
                        <asp:Label ID="shirtTypeLbl" AssociatedControlID="shirtType" runat="server" Text="Choose Shirt Type:"></asp:Label>
                        <asp:RadioButtonList id="shirtType" runat="server">
                            <asp:ListItem Value="CrewNeck" Text="Crew Neck Shirt" Selected="true"></asp:ListItem>
                            <asp:ListItem Value="VNeck" Text="V Neck Shirt"></asp:ListItem>
                            <asp:ListItem Value="YNeck" Text="Y Neck Shirt"></asp:ListItem>
                            <asp:ListItem Value="Polo" Text="Polo Shirt"></asp:ListItem>
                        </asp:RadioButtonList>
                 </div>
                 <div>
                    <!--
                     <asp:Label ID="shirtColorId" runat="server" Text="Choose Shirt Color:"></asp:Label>
                     <br/>
                    <asp:RadioButton runat="server" ID="clientShirtColor1" Value="R" Text="Red" GroupName="shirtColor" Checked="true"></asp:RadioButton>
                     <br/>
                    <asp:RadioButton runat="server" ID="clientShirtColor2" Value="B" Text="Blue" GroupName="shirtColor"></asp:RadioButton>
                     <br/>
                    <asp:RadioButton runat="server" ID="clientShirtColor3" Vavlue="G" Text="Green" GroupName="shirtColor"></asp:RadioButton>
                     <br/>
                    <asp:RadioButton runat="server" ID="clientShirtColor4" Value="W" Text="White" GroupName="shirtColor"></asp:RadioButton>
                     <br/>
                    <asp:RadioButton runat="server" ID="clientShirtColor5" Value="B" Text="Black" GroupName="shirtColor"></asp:RadioButton>

                        I will be changing all radio buttons to radio button list to retrieve the values easier
                        Source:
                            https://www.dotnetheaven.com/article/radiobuttonlist-control-in-asp.net
                            https://stackoverflow.com/questions/378620/how-to-get-the-selected-value-from-radiobuttonlist
                    -->
                        <asp:Label ID="shirtColorLbl" AssociatedControlID="shirtColor" runat="server" Text="Choose Shirt Color:"></asp:Label>
                        <asp:RadioButtonList id="shirtColor" runat="server">
                            <asp:ListItem Value="R" Text="Red" Selected="true"></asp:ListItem>
                            <asp:ListItem Value="B" Text="Blue"></asp:ListItem>
                            <asp:ListItem Value="G" Text="Green"></asp:ListItem>
                            <asp:ListItem Value="W" Text="White"></asp:ListItem>
                            <asp:ListItem Value="B" Text="Black"></asp:ListItem>
                        </asp:RadioButtonList>
                </div>
                <div>
                   <br/>
                    <!--
                    <asp:Label ID="shirtSizeId" runat="server" Text="Choose Shirt Size:"></asp:Label>
                    <br/>
                    <asp:RadioButton runat="server" ID="clientShirtSizeS" Value="S" Text="Small" GroupName="shirtSize" Checked="true"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="clientShirtSizeM" Value="M" Text="Medium" GroupName="shirtSize"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="clientShirtSizeL" Value="L" Text="Large" GroupName="shirtSize"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="clientShirtSizeXL" Value="XL" Text="Extra Large" GroupName="shirtSize"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="clientShirtsizeXXL" Value="XXL" Text="Double XL" GroupName="shirtSize"></asp:RadioButton>    
                    
                    I will be changing all radio buttons to radio button list to retrieve the values easier
                    Sources: 
                       https://www.dotnetheaven.com/article/radiobuttonlist-control-in-asp.net
                       https://stackoverflow.com/questions/378620/how-to-get-the-selected-value-from-radiobuttonlist
                   -->
                   <asp:Label ID="shirtSizeLbl" AssociatedControlID="shirtSize" runat="server" Text="Choose Shirt Size:"></asp:Label>
                   <asp:RadioButtonList id="shirtSize" runat="server">
                        <asp:ListItem Value="S" Text="Small" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="M" Text="Medium"></asp:ListItem>
                        <asp:ListItem Value="L" Text="Large"></asp:ListItem>
                        <asp:ListItem Value="XL" Text="Extra Large"></asp:ListItem>
                        <asp:ListItem Value="XXL" Text="Double XL"></asp:ListItem>
                  </asp:RadioButtonList>
                </div>
                <div>
                    <br/>
                    <!--
                    <asp:Label ID="mainDesignId" runat="server" Text="Primary Design Location"></asp:Label>
                    <br/>
                    <asp:RadioButton runat="server" ID="mainDesignFront" Value="F" Text="Front" GroupName="mainDesign" Checked="true"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="mainDesignBack" Value="B" Text="Back" GroupName="mainDesign"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="mainDesignLeft" Value="L" Text="Left Arm" GroupName="mainDesign"></asp:RadioButton>
                    <br/>
                    <asp:RadioButton runat="server" ID="mainDesignRight" Value="R" Text="Right Arm" GroupName="mainDesign"></asp:RadioButton>
                    
                    I will be changing all radio buttons to radio button list to retrieve the values easier
                    Sources: 
                       https://www.dotnetheaven.com/article/radiobuttonlist-control-in-asp.net
                       https://stackoverflow.com/questions/378620/how-to-get-the-selected-value-from-radiobuttonlist
                    -->
                   <asp:Label ID="mainDesignLbl" AssociatedControlID="mainDesign" runat="server" Text="Primary Design Location:"></asp:Label>
                   <asp:RadioButtonList id="mainDesign" runat="server">
                        <asp:ListItem Value="F" Text="Front" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="B" Text="Back"></asp:ListItem>
                        <asp:ListItem Value="L" Text="Left Arm"></asp:ListItem>
                        <asp:ListItem Value="R" Text="Right Arm"></asp:ListItem>
                  </asp:RadioButtonList>
                    <!--<p>Additional Design location</p>
                    <asp:Checkbox runat="server" ID="addDesignLocFront" Text="Front ( +$5.00 )"></asp:Checkbox>
                    <br/>
                    <asp:Checkbox runat="server" ID="addtDesignLocBack" Text="Back ( +$5.00 )"></asp:Checkbox>
                    <br/>
                    <asp:Checkbox runat="server" ID="addDesignLocLeft" Text="Left Arm ( +$3.00 )"></asp:Checkbox>
                    <br/>
                    <asp:Checkbox runat="server" ID="addDesignLocRight" Text="Right Arm ( +$3.00 )"></asp:Checkbox>
                    
                    I am changing checkbox to checkbox list as well to easily check the items that were checked
                    Resource: 
                        https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.checkboxlist?redirectedfrom=MSDN&view=netframework-4.7.2
                        -->
                    <br/>
                    <asp:Label ID="addDesignLocLbl" AssociatedControlID="addDesignLoc" runat="server" Text="Additonal Design Location (Optional):"></asp:Label>
                    <asp:CheckBoxList runat="server" ID="addDesignLoc">
                        <asp:ListItem Value="F" Text="Front ( +$5.00 )"></asp:ListItem>
                        <asp:ListItem Value="B" Text="Back ( +$5.00 )" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="L" Text="Left Arm ( +$3.00 )"></asp:ListItem>
                        <asp:ListItem Value="R" Text="Right Arm ( +$3.00 )"></asp:ListItem>
                    </asp:CheckBoxList>
                </div>
                <div>
                    <br/>
                    <asp:Label ID="fileLbl" runat="server" AssociatedControlID="clientFiles" Text="Upload your designs:"></asp:Label>
                    <asp:FileUpload runat="server" ID="clientFiles" AllowMultiple="true"></asp:FileUpload>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="You must upload your file" ControlToValidate="clientFiles" ID="fileUploadValidator" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ErrorMessage="File format not supported, use jpg and png files only" ControlToValidate="clientFiles" ValidationExpression="^.+\.((JPG)|(jPEG)|(JPEG)|(jpg)|(jpeg)|(png)|(PNG))$" ID="fileFormatValidator" ForeColor="Red"></asp:RegularExpressionValidator>
                    <!-- file format validation source: https://forums.asp.net/t/1252432.aspx?Check+file+type+with+custom+validator+Try+to+write+client+side+validation+function+ -->
                </div>
                <div>
                    <br/>
                    <asp:Label ID="designInstructionsLbl" runat="server" AssociatedControlID="designInstructions" Text="Design Instructions (if uploaded additional designs)"></asp:Label>
                    <br/>
                    <asp:TextBox ID="designInstructions" TextMode="MultiLine" runat="server"></asp:TextBox>
                </div>
                <div>
                    <br/>
                    <asp:Label ID="shirtQtyLbl" runat="server" AssociatedControlID="shirtQuantity" Text="Qty:"></asp:Label>
                    <asp:TextBox runat="server" ID="shirtQuantity" placeholder="Minimum of 1, Maximum of 10"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="You must order atleast 1" ControlToValidate="shirtQuantity" ID="quantityValidator1" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:RangeValidator runat="server" Type="Integer" MinimumValue="1" MaximumValue="10" ControlToValidate="shirtQuantity" ErrorMessage="You must order a minimum 1, and a maximum of 10" ID="quantityValidator2" ForeColor="Red"></asp:RangeValidator>
                </div>
          </fieldset>
        </section>
        <section>
            <fieldset>
                <legend>Payment</legend>
                  <div>
                    <!--<asp:RadioButton runat="server" ID="paypal" Text="Paypal" GroupName="payment" Checked="true"></asp:RadioButton>
                    <asp:RadioButton runat="server" ID="card" Text="Credit/Debit" GroupName="payment"></asp:RadioButton>
                    <asp:RadioButton runat="server" ID="emt" Text="EMT/Interact" GroupName="payment"></asp:RadioButton>
                                       
                   I will be changing all radio buttons to radio button list to retrieve the values easier
                    Sources: 
                       https://www.dotnetheaven.com/article/radiobuttonlist-control-in-asp.net
                       https://stackoverflow.com/questions/378620/how-to-get-the-selected-value-from-radiobuttonlist
                    -->
                   <asp:Label ID="paymentMethodLbl" runat="server" Text="Choose Payment Method:"></asp:Label>
                   <asp:RadioButtonList id="paymentMethod" runat="server">
                        <asp:ListItem Value="PP" Text="Paypal" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="CD" Text="Credit/Debit"></asp:ListItem>
                        <asp:ListItem Value="EMT" Text="EMT/Interact"></asp:ListItem>
                  </asp:RadioButtonList>
                </div>
                <div>
                    <br/>
                    <asp:Label ID="shipLbl" runat="server" AssociatedControlID="shipMethod" Text="Choose Shipping Method:"></asp:Label>
                    <asp:DropDownList runat="server" ID="shipMethod">
                        <asp:ListItem Value="DELIVERY" Text="Delivery"></asp:ListItem>
                        <asp:ListItem Value="PICKUP" Text="Pickup"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:CustomValidator runat="server" ID="customShipValidator" ErrorMessage="Sorry, pickups only available in ON" ControlToValidate="shipMethod" OnServerValidate="shippingMethodValidator" ForeColor="Red"></asp:CustomValidator>
                </div>
                <div>
                    <p>Order Status Tracking:</p>
                    <asp:Checkbox runat="server" ID="subscriptionMailChk" Text="I would like to sign up for mail updates"></asp:Checkbox>
                    <br/>
                    <asp:Checkbox runat="server" ID="subscriptionTxtChk" Text="I would like to sign up for text updates"></asp:Checkbox>
                </div>
            </fieldset>
           <br/>
           <asp:Button runat="server" ID="submitOrder" onClick="subOrder" Text="Submit Order" />
        </section>
            <!-- resources used:
        https://www.javatpoint.com/asp-net-textbox
        https://www.javatpoint.com/asp-net-web-form-rangevalidator
        https://www.javatpoint.com/asp-net-web-form-regular-expression-validator
        https://www.javatpoint.com/asp-net-web-form-validation-summary
        -->
    </form>
    <div id="orderSummary" runat="server"></div>
</body>
</html>
