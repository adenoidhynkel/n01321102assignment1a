﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class OrderShirt
    {
        public Client client;
        public Shirt shirt;
        public Logistics logistics;


        public OrderShirt(Client c, Shirt s, Logistics l)
        {
            client = c;
            shirt = s;
            logistics = l;
        }

        public double ProvincialRate()
        {
            string province = logistics.ClientProvince;
            double rate = 0.00;
            string shipMethod = logistics.DeliveryMethod;

            if (shipMethod.Equals("PICKUP") == true)
            {
                rate = 0.00;
            }
            else
            {
                switch (province)
                {
                    case "AB":
                        rate = 8.50;
                        break;
                    case "BC":
                        rate = 7.75;
                        break;
                    case "MB":
                        rate = 5.55;
                        break;
                    case "NB":
                        rate = 12.25;
                        break;
                    case "NL":
                        rate = 7.25;
                        break;
                    case "NS":
                        rate = 8.50;
                        break;
                    case "ON":
                        rate = 5.50;
                        break;
                    case "PE":
                        rate = 10.50;
                        break;
                    case "QC":
                        rate = 7.32;
                        break;
                    case "SK":
                        rate = 9.35;
                        break;
                }
            }

            return rate;
        }

        public double MainDesign()
        {
            double price = 0.00;
            char location = shirt.MainDesignLoc;

            switch (location)
            {
                case 'F':
                    price = 5.00;
                    break;
                case 'B':
                    price = 5.00;
                    break;
                case 'L':
                    price = 3.00;
                    break;
                case 'R':
                    price = 3.00;
                    break;
            }
            return price;
        }

        public double AddDesign()
        {
            double price = 0.00;
            List<Char> location = shirt.ShirtOtherLoc;

            foreach(char loc in location)
            {
                switch (loc)
                {
                    case 'F':
                        price += 5.00;
                        break;
                    case 'B':
                        price += 5.00;
                        break;
                    case 'L':
                        price += 3.00;
                        break;
                    case 'R':
                        price += 3.00;
                        break;
                    default:
                        price = 0.00;
                        break;
                }
            }

            return price;
        }

        public double PaymentGateway()
        {
            string gateway = logistics.PaymentMethod;
            double rate;

            switch (gateway)
            {
                case "PP":
                    rate = 0.04;
                    break;
                default:
                    rate = 0.00;
                    break;
            }
            return rate;
        }

        public double CheckOut()
        {
            double subtotal;
            double total;
            double mainDesPrice = MainDesign();
            double addOn = AddDesign();
            double provRateShipping = ProvincialRate();
            double paymentGatewayFee;
            double tax;
            double qty = shirt.ShirtQuantity;

            subtotal = (mainDesPrice + addOn) * qty;
            paymentGatewayFee = subtotal *  PaymentGateway();
            tax = subtotal * 0.13;
            total = subtotal + tax + paymentGatewayFee + provRateShipping;
            return total;
        }

        public string PrintSummary()
        {
            string summary = "";
            /*
            *shorthand if else for c#:
            *https://stackoverflow.com/questions/6073563/shorthand-if-statements-c-sharp
            */
            string txt = logistics.TextTracking == true ? "YES" : "NO";
            string email = logistics.EmailTracking == true ? "YES" : "NO";
            double subtotal = (MainDesign() + AddDesign()) * shirt.ShirtQuantity;
            double tax = subtotal * 0.13;
            double ppFee = subtotal * PaymentGateway();
            double provRateShipping = ProvincialRate();
            double tota1 = subtotal + tax + ppFee + provRateShipping;
            string deliveryAddress = "Delivery Address: " + logistics.ClientAddress + ", " + logistics.ClientCity + ", " + logistics.ClientProvince + " " + logistics.ClientZipCode + "<br/>";
            string addToSummary = logistics.DeliveryMethod.ToString().Equals("PICKUP") ? "" : deliveryAddress;

            summary += "<h2> Order Summary </h2>";
            summary += "<p> Name: " + client.ClientFName + " " + client.ClientLName + "<br/>";
            summary += "Email: " + client.ClientMail + "<br/>";
            summary += "Phone Number: " + client.ClientPhone + "<br/>";
            summary += "Shirt Type: " + shirt.ShirtType + "<br/>";
            summary += "Shirt Color: " + shirt.ShirtColor + "<br/>";
            summary += "Shirt Size: " + shirt.ShirtSize + "<br/>";
            summary += " Shirt Quantity: " + shirt.ShirtQuantity.ToString() + "<br/>";
            summary += "Main Design Location: " + shirt.MainDesignLoc + "<br/>";
            summary += "Additional Design Location: " + String.Join(", ", shirt.ShirtOtherLoc.ToArray()) + "<br/>";
            summary += "Additional Design Instruction: " + shirt.AddInstructions + "<br/>";
            summary += "Files uploaded: " + String.Join(", ", shirt.ShirtFiles.ToArray()) + "<br/>";
            summary += "Delivery Method: " + logistics.DeliveryMethod.ToString() + "<br/>";
            summary += addToSummary;
            summary += "Email Tracking: " + email + "<br/>";
            summary += "Text Tracking: " + txt + "<br/>";
            summary += "Payment Method: " + logistics.PaymentMethod + " Fee: $ " + ppFee.ToString() + " CAD<br/>";
            summary += "Main Design Cost: $ " + MainDesign().ToString() + " CAD<br/>";
            summary += "Additional Design Cost: $ " + AddDesign().ToString() + " CAD<br/>";
            summary += "Shipping Cost: $ " + ProvincialRate().ToString() + " CAD<br/>";
            summary += "Subtotal: $ " + subtotal.ToString() + " CAD<br/>";
            summary += "Tax: $ " + tax.ToString() + " CAD<br/>";
            summary += "Total: $ " + CheckOut().ToString() + " CAD</p>";

            return summary;
        }
    }
}