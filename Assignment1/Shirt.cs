﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Shirt
    {
        /*
         * shirt class
         */
        private string shirtType;
        private string theShirtColor;
        private string shirtSize;
        private Char mainLoc;
        private string addInstructions;
        private List<string> shirtFiles;
        private List<Char> shirtOtherLoc;
        private int shirtQty;

        public Shirt()
        {

        }

        public string ShirtType
        {
            get { return shirtType; }
            set { shirtType = value; }
        }

        public string ShirtColor
        {
            get { return theShirtColor; }
            set { theShirtColor = value; }
        }

        public string ShirtSize
        {
            get { return shirtSize; }
            set { shirtSize = value; }
        }

        public Char MainDesignLoc
        {
            get { return mainLoc; }
            set { mainLoc = value; }
        }

        public string AddInstructions
        {
            get { return addInstructions; }
            set { addInstructions = value; }
        }

        public List<string> ShirtFiles
        {
            get { return shirtFiles; }
            set { shirtFiles = value; }
        }

        public List<Char> ShirtOtherLoc
        {
            get { return shirtOtherLoc; }
            set { shirtOtherLoc = value; }
        }

        public int ShirtQuantity
        {
            get { return shirtQty; }
            set { shirtQty = value; }
        }
    }
}